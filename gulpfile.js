var gulp = require('gulp');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var fs = require('fs');
var jsforce = require('jsforce');

//skynet@skynet.ca -p Tr41n1ng48nLVAXwjGH1wJlCr79nhJx1

gulp.task('test',function() {
    
    var username = "skynet@skynet.ca";
    var password = "Tr41n1ng48nLVAXwjGH1wJlCr79nhJx1";

    var conn = new jsforce.Connection({
    // you can change loginUrl to connect to sandbox or prerelease env.
    // loginUrl : 'https://test.salesforce.com'
    });
    conn.login(username, password, function(err, userInfo) {
    if (err) { return console.error(err); }
    // Now you can get the access token and instance URL information.
    // Save them to establish connection next time.
    console.log(conn.accessToken);
    console.log(conn.instanceUrl);
    // logged in user property
    console.log("User ID: " + userInfo.id);
    console.log("Org ID: " + userInfo.organizationId);
    // ...
    });
    
});

gulp.task('watch', function() {
    gulp.watch(['./src/package.xml','./src/classes/*.*'], function(event){
        console.log(event);
        gulp.start('deploy');
    });
});

gulp.task('zip-slds',function(){
    gulp.src('./bower_components/salesforce-lightning-design-system/assets/**')
    .pipe(zip('slds.resource'))
    .pipe(gulp.dest('./src/staticresources'));
});

gulp.task('deploy',function() {
    return gulp.src(['./src/package.xml',
    './src/classes/*.*',
    './src/staticresources/*.*'
    ],{base: '.'})
    .pipe(zip('pkg.zip'))
    .pipe(forceDeploy({
        username: 'skynet@skynet.ca',
        password: 'Tr41n1ng48nLVAXwjGH1wJlCr79nhJx1',
        loginUrl: 'https://login.salesforce.com',
        version: '36.0'
    }));
});

// define the default task and add the watch task to it
gulp.task('default', ['watch']);